# Bank-end du WebApp Life Style--Version2

*Projet réalisé par :* 

   * Shokoufeh  AHMADI SIMAB
   * Premiere version: [https://gitlab.com/NacimaG/life_style] avec NacimaG


*Access au Front:*
    [https://gitlab.com/shokufe/lifeStyle_v2]

*Base de données : le fichier lifeStyle1.sql

*Requirement :*\
    -Java13\
    -ServerApplication comme Tomcat9\
    -MySQl(mettre les configurations de votre BDD dans la classe BaseDD.DBStatic )\

*features ajoutée in V2:*\
    - signup:\
        + Changée l'age a date de naissance\
        + Check email pour l'inscription \
        + Control confirmed password \
        + Ajoute different type de Event \
    - Ajoute un nouveau Servlet (listUserEvent) \
        + pour chaque utilisateur ils peuvent voir leurs liste des event correspondant \
        + join a cet event \
    - Ajoute Servlet de getUserProfile \
        + renvoie des info de User selon de leurs friendship\
    - Ajoute Servlet de Notification  \
        + trouver les evenment joint proche d'aujourd'hui     



    
