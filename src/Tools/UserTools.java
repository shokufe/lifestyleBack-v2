package Tools;

import java.sql.Connection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import BaseDD.Database;

/**
 * Classe d'outils reli� � la gestion d'un utilisateur
 * 
 * @author Nassima GHOUT et Shokoufeh AHMADI SIMAB
 *
 */
public class UserTools {
	/**
	 * 
	 * @param login
	 * @param password
	 * @param nom
	 * @param prenom
	 * @param sexe
	 * @param age
	 * @param mail
	 * @param phone
	 * @param poids
	 * @param taille
	 * @param bmi
	 * @param adress
	 * @param zipcode
	 * @throws ParseException 
	 */
	public static void InsertUser(String login, String password, String nom, String prenom, String sexe, String age,
			String mail, String phone, int poids, int taille, double bmi, String adress, String zipcode, String country) throws ParseException {
		try {
			Connection connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();

			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date = formatter.parse(age);
			System.out.println("age"+age);
			
			System.out.println("date"+date);
			java.sql.Date sqlDate = new java.sql.Date(date.getTime());
			String query = "INSERT INTO user(user_login,user_password,nom,prenom,sexe,dateDeN,mail,phoneNumber,poids,taille,bmi,adresse,ZipCode,country) VALUES('"
					+ login + "','" + password + "','" + nom + "','" + prenom + "','" + sexe + "','" + sqlDate + "','"
					+ mail + "','" + phone + "','" + poids + "','" + taille + "','" + bmi + "','" + adress + "','"
					+ zipcode +"','"+country+"');";
			System.out.println(query);
			int resultat = statement.executeUpdate(query);
			System.out.println("executed update");
			statement.close();
			connexion.close();
			System.out.println(resultat);

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @param login
	 * @return
	 */

	public static String InsertConnexion(String login) {
		UUID uuid = UUID.randomUUID();
		String key = Long.toString(uuid.getMostSignificantBits(), 36)
				+ Long.toString(uuid.getLeastSignificantBits(), 36);
		;

		try {
			Timestamp fin_session = new Timestamp(System.currentTimeMillis() + (((3600) + 59) * 1000));
			Connection connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String query = "INSERT INTO session(session_key,user_login,session_fin) VALUES('" + key + "','" + login
					+ "','" + fin_session + "');";
			int resultat = statement.executeUpdate(query);
			statement.close();
			connexion.close();
			System.out.println(resultat);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return key;
	}

	public static String getLogin(int id) {
		String log = "";
		try {
			Connection connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String req = "SELECT user_login FROM user WHERE user_id= '" + id + "'";
			ResultSet res = statement.executeQuery(req);

			if (res.next())
				return res.getString(1);
			statement.close();
			connexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return log;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public static String getLogSession(String key) {
		String log = "";
		try {
			Connection connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String req = "SELECT user_login FROM session WHERE session_key= '" + key + "'";
			ResultSet res = statement.executeQuery(req);

			if (res.next())
				return res.getString(1);
			statement.close();
			connexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return log;
	}
	public static int getUserId(String login) {
		int UserID = 0;
		try {
			Connection connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String req = "SELECT user_id FROM user WHERE user_login= '" + login + "'";
			ResultSet res = statement.executeQuery(req);

			if (res.next())
				return Integer.parseInt(res.getString(1));
			statement.close();
			connexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return UserID;
	}
}
