package Tools;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import BaseDD.Database;

/**
 * Classe d'outils reli� � la gestion de Comments
 * 
 * @author Nassima GHOUT et Shokoufeh AHMADI SIMAB
 *
 */
public class CommentTools {
	/**
	 * 
	 * @param idComment
	 * @return
	 */
	public static boolean checkComment(int idComment) {
		try {
			Connection connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String query = "SELECT * FROM Comment WHERE idComment = '" + idComment + "'";
			System.out.println(1);
			ResultSet resultat = statement.executeQuery(query);
			System.out.println(2);
			boolean res = resultat.next();
			resultat.close();
			statement.close();
			connexion.close();

			return res;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;

	}

	public static int getCommenter(int idComment) {
		int res=0;

		try {
			Connection connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String query = "SELECT user_id FROM Comment WHERE idComment = '" + idComment + "'";
			ResultSet resultat = statement.executeQuery(query);
			if (resultat.next())
				res= Integer.parseInt(resultat.getString(1));
			
			resultat.close();
			statement.close();
			connexion.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

}
