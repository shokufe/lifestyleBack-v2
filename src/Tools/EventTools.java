package Tools;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.JSONException;
import org.json.JSONObject;

import BaseDD.Database;

/**
 * Classe d'outils reli� � la gestion d'�v�nements
 * 
 * @author Nassima GHOUT et Shokoufeh AHMADI SIMAB
 *
 */
public class EventTools {
	/**
	 * 
	 * @param name
	 * @return
	 */
	public static int get_userId_ev(String name) {
		int id_user = 0;
		try {
			Connection connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String req = "SELECT user_id FROM user WHERE name= '" + name + "'";
			ResultSet res = statement.executeQuery(req);

			if (res.next())
				return Integer.parseInt(res.getString(1));
			statement.close();
			connexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return id_user;
	}

	/**
	 * 
	 * @param name
	 * @return
	 */
	public static int get_eventId(String name) {
		int id_user = 0;
		try {
			Connection connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String req = "SELECT event_id FROM user WHERE name= '" + name + "'";
			ResultSet res = statement.executeQuery(req);

			if (res.next())
				return Integer.parseInt(res.getString(1));
			statement.close();
			connexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return id_user;
	}

	/**
	 * 
	 * @param name
	 * @param event_id
	 * @param user_id
	 * @return
	 * @throws SQLException
	 */
	public static boolean checkEvent(String name, int event_id, int user_id) throws SQLException {

		Connection connexion = Database.getMySQLConnection();
		Statement statement = connexion.createStatement();
		String query = "SELECT name FROM events WHERE name ='" + name + "' and event_id = '" + event_id
				+ "' and user_id = '" + user_id + "' ;";
		ResultSet resultat = statement.executeQuery(query);

		boolean res = resultat.next();
		resultat.close();
		statement.close();
		connexion.close();

		return res;

	}

	/**
	 * 
	 * @param name
	 * @param user_id
	 * @return
	 * @throws SQLException
	 */
	public static boolean checkEv(String name, int user_id) throws SQLException {

		Connection connexion = Database.getMySQLConnection();
		Statement statement = connexion.createStatement();
		String query = "SELECT name FROM events WHERE name ='" + name + "' and user_id = '" + user_id + "' ;";
		ResultSet resultat = statement.executeQuery(query);

		boolean res = resultat.next();
		resultat.close();
		statement.close();
		connexion.close();

		return res;

	}

	/**
	 * 
	 * @param owner
	 * @param name
	 * @return
	 */
	public static JSONObject getEventInfos(String owner, String name) {
		JSONObject jo = new JSONObject();
		Connection connexion;
		try {
			connexion = Database.getMySQLConnection();

			connexion = (Connection) Database.getMySQLConnection();
			Statement statement = (Statement) connexion.createStatement();
			String query = "SELECT * FROM events WHERE owner = '" + owner + "' and name = '" + name + "';";
			ResultSet resultat = statement.executeQuery(query);
			while (resultat.next()) {

				jo.put("event_id", resultat.getString(1));
				jo.put("owner", resultat.getString(3));
				jo.put("name", resultat.getString(4));
				jo.put("category", resultat.getString(5));
				jo.put("date", resultat.getString(6));
				jo.put("lieu", resultat.getString(7));
				
				jo.put("status", resultat.getString(9));
			}

			resultat.close();
			statement.close();
			connexion.close();
		} catch (SQLException s) {
			s.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jo;
	}

	public static String getOwner(String idEv) {
		String owner = "";
		Connection connexion;
		try {
			connexion = Database.getMySQLConnection();

			connexion = (Connection) Database.getMySQLConnection();
			Statement statement = (Statement) connexion.createStatement();
			String query = "SELECT owner FROM events WHERE event_id = '" + idEv + "';";
			ResultSet resultat = statement.executeQuery(query);
			while (resultat.next()) {
				owner = resultat.getString(1);

			}
			resultat.close();
			statement.close();
			connexion.close();
		} catch (SQLException s) {
			s.printStackTrace();
		}
		return owner;

	}
	public static boolean checkEvID(int event_id) throws SQLException {

		Connection connexion = Database.getMySQLConnection();
		Statement statement = connexion.createStatement();
		String query = "SELECT * FROM events WHERE event_id = '" + event_id +" ';";
		ResultSet resultat = statement.executeQuery(query);

		boolean res = resultat.next();
		resultat.close();
		statement.close();
		connexion.close();

		return res;

	}

}
