package Servlets.Comments;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Services.Comments;

public class NewComment extends HttpServlet {

	/**
	 * serlet permettanat de publier un nouveau post on a besoin de la cl� de
	 * connexion de l'utilisateur et du message � publier
	 * @author Nassima GHOUT et Shokoufeh AHMADI SIMAB
	 */
	private static final long serialVersionUID = 1L;

	public NewComment() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType(" text / plain ");
		String key = request.getParameter("key");
		String idEvent = request.getParameter("idEvent");
		String content = request.getParameter("content");
		out.println((Comments.addComment(key,idEvent, content)));

	}

}
