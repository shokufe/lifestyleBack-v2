package Servlets.Comments;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Services.Comments;

public class ListComments extends HttpServlet {
	/**
	 * servlet permettant d'afficher tout les posts publi�s par l'utilisateur en
	 * utilisant son login
	 * 
	 * @author Nassima GHOUT et Shokoufeh AHMADI SIMAB
	 */
	private static final long serialVersionUID = 1L;

	public ListComments() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType(" text / plain ");
		PrintWriter out = response.getWriter();
		String idEvent = request.getParameter("idEvent");
		out.println(Comments.listComments(idEvent));
	}

}
