package Servlets.User;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Services.User;

public class GetUserProfile extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public GetUserProfile() {
		super();
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();
		String log = request.getParameter("login");
		String key =request.getParameter("key");
		out.println(User.UserInfo(log,key));
		
	}

}
