package Servlets.User;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Services.User;

public class Logout extends HttpServlet {
	/**
	 * servlet g�rant la d�connexion d'un utilisateur avec la cl� de connexion
	 * @author Nassima GHOUT et Shokoufeh AHMADI SIMAB
	 */
	private static final long serialVersionUID = 1L;

	public Logout() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("text /plain");
		PrintWriter out = response.getWriter();
		String key = request.getParameter("key");
		out.println(User.logout(key));
	}
}
