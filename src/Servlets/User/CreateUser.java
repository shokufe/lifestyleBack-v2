package Servlets.User;

import java.io.IOException;
import java.io.PrintWriter;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Services.User;

public class CreateUser extends HttpServlet {
	/**
	 * servlet permettant l'inscription d'un utilisateur et son ajout dans la base
	 * de donn�es � partir de ses coordonn�es
	 * 
	 *  @author Nassima GHOUT et Shokoufeh AHMADI SIMAB
	 */
	private static final long serialVersionUID = 1L;

	public CreateUser() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();

		String log = request.getParameter("login");
		String psw = request.getParameter("password");
		String psw2 = request.getParameter("password2");
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String sexe = request.getParameter("sexe");
		//String age = request.getParameter("age");
		String dateDeN = request.getParameter("dateDeN");
		String mail = request.getParameter("mail");
		String phone = request.getParameter("phone");
		String poids = request.getParameter("poids");
		String taille = request.getParameter("taille");
		String adress = request.getParameter("adresse");
		String zipcode = request.getParameter("ZipCode");
		String country = request.getParameter("country");

	
			out.println(User.createUser(log, psw,psw2, nom, prenom, sexe, dateDeN, mail, phone, poids, taille, adress, zipcode,country));
		
			// TODO Auto-generated catch block
		
	

	}

}
