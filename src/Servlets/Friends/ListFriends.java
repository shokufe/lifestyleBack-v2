package Servlets.Friends;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Services.Friends;

public class ListFriends extends HttpServlet {
	/**
	 * afficher liste d'amis
	 * 
	 * @author Nassima GHOUT et Shokoufeh AHMADI SIMAB
	 */
	private static final long serialVersionUID = 1L;

	public ListFriends() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType(" text / plain ");
		PrintWriter out = response.getWriter();
		String key = request.getParameter("key");
		out.println(Friends.listFriend(key));

	}

}