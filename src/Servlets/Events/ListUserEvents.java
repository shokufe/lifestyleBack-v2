package Servlets.Events;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ListUserEvents extends HttpServlet{
	public ListUserEvents() {
		super();
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();
		String key = request.getParameter("key");
		String userId = request.getParameter("userId");
		out.println(Services.Events.listUserEvents(key,userId));
	}

}
