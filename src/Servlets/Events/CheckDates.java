package Servlets.Events;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import Services.CheckEvents;


/**
 * Check les Dates des events 
 * 
 * @author Nassima GHOUT et Shokoufeh AHMADI SIMAB
 */

public class CheckDates extends HttpServlet{
	private static final long serialVersionUID = 1L;

	public CheckDates() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType(" text / plain ");
		PrintWriter out = response.getWriter();
		String key = request.getParameter("key");
			out.println(CheckEvents.checkExpiration(key));
	
	}
}
