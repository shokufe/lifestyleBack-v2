package Servlets.Events;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import Services.Events;
/**
 * modifie un event
 * 
 * @author Shokoufeh AHMADI SIMAB, Nassima GHOUT 
 */
public class EditEvent extends HttpServlet{

	
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		PrintWriter out;
		try {
			out = response.getWriter();
		
		response.setContentType(" text / plain ");
		
		String id =request.getParameter("id");
		String key =  request.getParameter("key");
		String name = request.getParameter("name");
		String type = request.getParameter("type");
		String date = request.getParameter("date");
		String lieu = request.getParameter("lieu");
		JSONObject j =Events.editEvent(id,key,name, type, date, lieu); 
		System.out.println(j);
		out.println(j);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	
	}

}
