package Servlets.Profil;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
/**
 *Recupere les info d'un API externe pour les exercises
 * 
 * @author Shokoufeh AHMADI SIMAB, Nassima GHOUT 
 */

import Services.Profil;

public class ShowExercise extends HttpServlet{

	public ShowExercise() {
		super();
	}
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter ();
	 	response.setContentType( " text / plain " );
		String pageNum2 = request.getParameter("pageNum");
		
			try {
				//JSONObject j =Profil.exerciseInfo(pageNum2); 
				System.out.println(Profil.exerciseInfo(pageNum2));// );
				out.println(Profil.exerciseInfo(pageNum2));
			} catch (JSONException e) {
				e.printStackTrace();
			}

	}

}
