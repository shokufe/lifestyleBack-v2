package Services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import BaseDD.Database;
import Tools.ConnexionTools;
import Tools.ErrorJson;
import Tools.EventTools;
import Tools.UserTools;

/**
 * Services reli� � la gestion d'un utilisateur
 * 
 * @author Shokoufeh AHMADI SIMAB et Nassima GHOUT
 *
 */
public class Events {



	/**
	 * AddEvent Creation et insertion d'un event dans la BD Verification
	 * que les arguments sont pas null et que login n'existe pas d�j�
	 * 
	 * @param name
	 * @param type
	 * @param date
	 * @param lieu
	 * @param key

	 * @return JSONObject
	 */
	public static JSONObject addEvent(String name, String category, String date, String lieu, String key) {
		JSONObject retour = new JSONObject();
		try {
			if (key == null)
				return Tools.ErrorJson.serviceRefused("missing arguments", -1);

			if (!ConnexionTools.checkSession(key))
				return Tools.ErrorJson.serviceRefused("Connexion non existante", -10);
			if (lieu == null)
				return Tools.ErrorJson.serviceRefused("Lieu non renseigne", -2);

			Connection connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String login = UserTools.getLogSession(key);
			int id = ConnexionTools.getId(login);

			if (EventTools.checkEv(name, id))
				return Tools.ErrorJson.serviceRefused("already exists", 1);
			
			LocalDateTime now = LocalDateTime.now();
			String query = "INSERT INTO events(owner,name, category, user_id, date, lieu , created_at ,status) VALUES('" + login
					+ "','" + name + "','" + category + "','" + id + "','" + date + "','" + lieu + "','" + now + "',"+ 1 + ");";
			statement.executeUpdate(query);
			statement.close();
			connexion.close();

			retour = Tools.ErrorJson.serviceAccepted();

		} catch (JSONException e) {
			e.printStackTrace();
		} catch (SQLException s) {
			s.printStackTrace();
		}

		return retour;
	}

	/**
	 * supprimer un �v�nement
	 * 
	 * @param idEvent,key
	 * @return
	 */
	public static JSONObject removeEvent(String key, String idEvent) {
		JSONObject retour = new JSONObject();

		try {
			if (key == null)
				return Tools.ErrorJson.serviceRefused("missing arguments", -1);

			if (!ConnexionTools.checkSession(key))
				return Tools.ErrorJson.serviceRefused("Connexion non existante", -10);
			if ((idEvent == null))
				return Tools.ErrorJson.serviceRefused("Connexion non existante", -1);

			String login = UserTools.getLogSession(key);
			if (!EventTools.getOwner(idEvent).equals(login))
				return Tools.ErrorJson.serviceRefused("vous n'avez pas le droit de supprimer l'�v�nement", -2);

			Connection connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String query = "DELETE FROM events WHERE event_id= '" + idEvent + "' and owner = '" + login + "';";

			statement.executeUpdate(query);
			statement.close();
			connexion.close();

			retour = Tools.ErrorJson.serviceAccepted();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (SQLException s) {
			s.printStackTrace();
		}
		return retour;
	}

	/**
	 * lister tout les �v�nement valide
	 * 
	 * @return
	 */
	public static JSONArray listAllEvents() {
		JSONArray events = new JSONArray();
		System.out.println("HELLO");
		try {
			Connection connexion;
			connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String query = "SELECT DISTINCT  event_id, owner, name, category,date, lieu  FROM events WHERE status ="+ 1 +"" ;
			ResultSet resultat = statement.executeQuery(query);
			while (resultat.next()) {
				JSONObject jo = new JSONObject();
				jo = Tools.ErrorJson.serviceAccepted();
				jo.put("event_id", resultat.getString(1));
				//jo.put("user_id", resultat.getString(2));
				jo.put("owner", resultat.getString(2));// 
				jo.put("name", resultat.getString(3));// ok
				jo.put("date", resultat.getString(5));// 3
				jo.put("lieu", resultat.getString(6));// ok
				jo.put("category", resultat.getString(4));
				events.put(jo);
			}
			resultat.close();
			statement.close();
			connexion.close();
		} catch (JSONException j) {
			j.printStackTrace();
		} catch (SQLException s) {
			s.printStackTrace();
		}
		return events;
	}
	/**
	 * joinEvent permet d'un utilisateur de joint a un event
	 * 
	 * @param name
	 * @param owner
	 * @param key

	 * @return JSONObject
	 */
	public static JSONObject joinEvent(String name, String owner, String key) {
		JSONObject retour = new JSONObject();
		try {

			if ((key == null) || (owner == null))
				return Tools.ErrorJson.serviceRefused("missing arguments", -1);

		/*	if (!ConnexionTools.checkSession(key))
				return Tools.ErrorJson.serviceRefused("Connexion non existante", -10);*/

			if (!EventTools.checkEv(name, ConnexionTools.getId(owner)))
				return ErrorJson.serviceRefused("Event not exists ", -1);
			JSONObject infos = EventTools.getEventInfos(owner, name);

			int event_id = Integer.parseInt((infos.getString("event_id")));
			int user_id = ConnexionTools.getId(UserTools.getLogSession(key));
			String owner_e = infos.getString("owner");
			String category = infos.getString("category");
			String date = infos.getString("date");
			String lieu = infos.getString("lieu");
			String status = infos.getString("status");

			Connection connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String login = UserTools.getLogSession(key);
			int id = ConnexionTools.getId(login);
			if (EventTools.checkEv(name, id))
				return Tools.ErrorJson.serviceRefused("already exists", 1);

			LocalDateTime now = LocalDateTime.now();
			String query = "INSERT INTO events(event_id,user_id,owner,name, category, date, lieu , created_at, status) " + "VALUES("
					+ event_id + ",'" + user_id + "','" + owner_e + "','" + name + "','" + category + "','" + date + "','"
					+ lieu + "','" + now +"','" + status + "');";
			statement.executeUpdate(query);
			statement.close();
			connexion.close();

			retour = Tools.ErrorJson.serviceAccepted();

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return retour;
	}
	
	/**
	 * listMyEvet recuperer les event que user a cree
	 * @param key
	 * @return JSONArray lists des events
	 */
	public static JSONArray listMyEvents(String key) {
		JSONArray events = new JSONArray();

		JSONObject retour = new JSONObject();
		try {
			if (key == null) 
				return events.put(Tools.ErrorJson.serviceRefused("Pas d'argument", -1));
			if (!ConnexionTools.checkSession(key))
				return events.put(Tools.ErrorJson.serviceRefused("connexion not existante", 1));
			String login = UserTools.getLogSession(key);
			Connection connexion;

			connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();

			String query = "SELECT * FROM events WHERE owner= '" + login + "'And status="+1+"";
			System.out.println(query);
			ResultSet resultat = statement.executeQuery(query);
						

			while (resultat.next()) {
				// System.out.println("nom::"+resultat.getString(3));
				JSONObject jo = new JSONObject();
				jo = Tools.ErrorJson.serviceAccepted();
				jo.put("id", resultat.getString(1));
				jo.put("owner", resultat.getString(3));// 
				jo.put("name", resultat.getString(4));// ok
				jo.put("date", resultat.getString(6));// 3
				jo.put("lieu", resultat.getString(7));// ok
				jo.put("created_at", resultat.getString(8));// ok
				events.put(jo);
				
			}
			// jo.put("user", resultat.getString(3));
			resultat.close();
			statement.close();
			connexion.close();

		} catch (JSONException j) {
			j.printStackTrace();
		} catch (SQLException s) {
			s.printStackTrace();
		}
		System.out.println(events);
		return events;
	}
	/**
	 * countPartEvent compter les nombre de participant a une event
	 * @param idEvent
	 * @return JSONObject nombre des participant
	 */
	public static JSONObject countPartEvents(String idEvent) {
		JSONObject retour = new JSONObject();
		try {
		if(idEvent == null)
			return Tools.ErrorJson.serviceRefused("missing arguments", -1);
		int idEv = Integer.parseInt(idEvent);
		if (!EventTools.checkEvID(idEv))
			return Tools.ErrorJson.serviceRefused("event not exists", -10);
		Connection connexion = Database.getMySQLConnection();
		Statement statement = connexion.createStatement();
		String req = "SELECT COUNT(*) FROM events WHERE event_id = '"+ idEv + "' ;";
		ResultSet res = statement.executeQuery(req);
		retour = Tools.ErrorJson.serviceAccepted();
		if (res.next())
			retour.put("nbParticipants", Integer.parseInt(res.getString(1)));
		
		statement.close();
		connexion.close();
		
		
		}catch (JSONException j) {
			j.printStackTrace();
		}catch (SQLException s) {
			s.printStackTrace();
		}
		// TODO Auto-generated method stub
		return retour;
	}
	/**
	 * CountCreated compter les nombre de Event que j' a cree
	 * @param key
	 * @return JSONObject nombre des event que j'ai cree
	 */
	public static JSONObject CountCreated(String key) {
		// TODO Auto-generated method stub
		JSONObject jo = new JSONObject();
		try {
			if (key == null) 
				return Tools.ErrorJson.serviceRefused("Pas d'argument", -1);
			if (!ConnexionTools.checkSession(key))
				return Tools.ErrorJson.serviceRefused("connexion not existante", 1);
			
			String login = UserTools.getLogSession(key);
			//int idUser= UserTools.getUserId(login);
			Connection connexion;
			connexion = (Connection) Database.getMySQLConnection();
			Statement statement = (Statement) connexion.createStatement();
			String query = "SELECT COUNT(*) FROM events WHERE owner='"+login+"'";
			ResultSet resultat = statement.executeQuery(query);
			//System.out.println(">>>"+resultat);
			jo.append("Statue", "OK");
			jo.append("code", 200);
			while (resultat.next()) {
				jo.append("CeatedEvent",resultat.getString(1));

			}
			resultat.close();
			statement.close();
			connexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		}
		return jo;
	}
	/**
	 * CountJoin compter les nombre de Event que j' a joint
	 * @param key
	 * @return JSONObject nombre des event que j'ai joint
	 */
	public static JSONObject CountJoin(String key) {
		JSONObject jo = new JSONObject();
		try {
			if (key == null) 
				return Tools.ErrorJson.serviceRefused("Pas d'argument", -1);
			if (!ConnexionTools.checkSession(key))
				return Tools.ErrorJson.serviceRefused("connexion not existante", 1);
			
			String login = UserTools.getLogSession(key);
			int idUser= UserTools.getUserId(login);
			Connection connexion;
			connexion = (Connection) Database.getMySQLConnection();
			Statement statement = (Statement) connexion.createStatement();
			String query = "SELECT COUNT(*) FROM events WHERE user_id="+idUser+" AND owner !='"+login+"'";
			ResultSet resultat = statement.executeQuery(query);
			jo.append("Statue", "OK");
			jo.append("code", 200);
			while (resultat.next()) {
				jo.put("JointEvent",resultat.getString(1));

			}
			resultat.close();
			statement.close();
			connexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		}
		return jo;
	}
	/**
	 * editEvent modifie les info d'un Event
	 * @param id
	 * @param key
	 * @param namenew
	 * @param typenew
	 * @param datenew
	 * @param  lieunew
	 * @return JSONObject nombre des event que j'ai cree
	 */
	public static JSONObject editEvent(String id, String key, String namenew, String typenew, String datenew, String lieunew) {
		JSONObject retour = new JSONObject();
		if (key == null )
			try {
				return Tools.ErrorJson.serviceRefused("Key is null", -1);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		if( id==null)
			try {
				return Tools.ErrorJson.serviceRefused("id is null", -3);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				
			}
		if (lieunew == null ||datenew==null||namenew ==null||typenew==null)
			try {
				return Tools.ErrorJson.serviceRefused("one field is empty", -2);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		if (!ConnexionTools.checkSession(key))
			try {
				return Tools.ErrorJson.serviceRefused("You cannot edit this event", 1);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		String LogIN = UserTools.getLogSession(key);
		int idINT=Integer.parseInt(id);
		int userID=UserTools.getUserId(LogIN);
		Connection connexion;
		JSONObject jo = new JSONObject();
		try {
			connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			
			String query = "UPDATE events SET owner='"+ LogIN+"', name='"+namenew+"',type='"+typenew+"',date='"+datenew+"',lieu='"+lieunew+"' WHERE "
					+ "event_id= " + idINT + " AND user_id="+userID+" ";
					System.out.println(">>>"+query);
					boolean resultat = statement.execute(query);
					System.out.println(">>>result ::"+resultat);
			statement.close();
			connexion.close();
		}catch (SQLException e) {
		e.printStackTrace();
	}
		retour=jo;
		return retour;
	}

	public static JSONArray listMyJointEvents(String key) {
		JSONArray events = new JSONArray();

		JSONObject retour = new JSONObject();
		try {
			if (key == null) 
				return events.put(Tools.ErrorJson.serviceRefused("Pas d'argument", -1));
			if (!ConnexionTools.checkSession(key))
				return events.put(Tools.ErrorJson.serviceRefused("connexion not existante", 1));
			String login = UserTools.getLogSession(key);
			int userId=UserTools.getUserId(login);
			Connection connexion;

			connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();

			String query = "SELECT * FROM events WHERE owner!= '" + login + "'And status="+1+" And user_id="+userId+"";
			System.out.println(query);
			ResultSet resultat = statement.executeQuery(query);
			while (resultat.next()) {
				JSONObject jo = new JSONObject();
				jo = Tools.ErrorJson.serviceAccepted();
				jo.put("id", resultat.getString(1));
				jo.put("owner", resultat.getString(3));// 
				jo.put("name", resultat.getString(4));// ok
				jo.put("date", resultat.getString(6));// 3
				jo.put("lieu", resultat.getString(7));// ok
				jo.put("created_at", resultat.getString(8));// ok
				events.put(jo);
				
			}
			resultat.close();
			statement.close();
			connexion.close();

		} catch (JSONException j) {
			j.printStackTrace();
		} catch (SQLException s) {
			s.printStackTrace();
		}
		System.out.println(events);
		return events;
	}

	public static JSONArray listUserEvents(String key, String userId) {
		JSONArray events = new JSONArray();
		try {
			if (key == null) 
				return events.put(Tools.ErrorJson.serviceRefused("Pas d'argument", -1));
			if (!ConnexionTools.checkSession(key))
				return events.put(Tools.ErrorJson.serviceRefused("connexion not existante", 1));
			Connection connexion;
			connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			int user_id=Integer.parseInt(userId);
			String query = "SELECT DISTINCT  event_id, owner, name, category,date, lieu  FROM events WHERE user_id= "+user_id+" And status ="+ 1 +"" ;
			ResultSet resultat = statement.executeQuery(query);
			while (resultat.next()) {
				JSONObject jo = new JSONObject();
				jo = Tools.ErrorJson.serviceAccepted();
				jo.put("event_id", resultat.getString(1));
				//jo.put("user_id", resultat.getString(2));
				jo.put("owner", resultat.getString(2));// 
				jo.put("name", resultat.getString(3));// ok
				jo.put("date", resultat.getString(5));// 3
				jo.put("lieu", resultat.getString(6));// ok
				jo.put("category", resultat.getString(4));
				events.put(jo);
			}
			resultat.close();
			statement.close();
			connexion.close();
		} catch (JSONException j) {
			j.printStackTrace();
		} catch (SQLException s) {
			s.printStackTrace();
		}
		return events;
	}

	public static boolean NearDate(String event_date) {
		// TODO Auto-generated method stub
	
		 try {
			//today
			 Date current = new Date();
			 Calendar cal = Calendar.getInstance();
			 cal.setTime(current);
			 int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
			// int month = cal.get(Calendar.MONTH);
			// int year = cal.get(Calendar.YEAR);
			 //event day
			 Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(event_date);
			 Calendar cal2 = Calendar.getInstance();
			 cal2.setTime(date1);
			 int dayOfEvent =cal2.get(Calendar.DAY_OF_MONTH);
			
			if(date1.before(current))
			{
				if((dayOfEvent-dayOfMonth==1) || (dayOfEvent-dayOfMonth==0)) {
					System.out.println("event--"+dayOfEvent);
					System.out.println("month--"+dayOfMonth);
					return true;
				}
				
			}else {
				return false;
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return false;
	}

	}




