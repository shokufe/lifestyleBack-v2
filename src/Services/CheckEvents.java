package Services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mysql.jdbc.SQLError;

import BaseDD.Database;
/**
 * Services reli� � la gestion d'un utilisateur
 * 
 * @author Shokoufeh AHMADI SIMAB et Nassima GHOUT
 *
 */
public class CheckEvents {
	/**
	 * checkExpiration chercher pour les events expirer
	 * @param key
	 * @return JSONObject code
	 */
	public static JSONObject checkExpiration(String key)  {
		JSONObject ret = new JSONObject();
		JSONArray events = new JSONArray();
		Connection connexion;
		Date date1;
		Date dt1=new Date();
		//String event_id;
		
		try {
			if  (key == null) 
				return Tools.ErrorJson.serviceRefused("missing arguments", -1);
			connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String query = "SELECT DISTINCT event_id , date FROM events WHERE status=1 ";
			ResultSet resultat =statement.executeQuery(query);
			while (resultat.next()) {
				JSONObject jo = new JSONObject();
				jo = Tools.ErrorJson.serviceAccepted();
				jo.put("event_id", resultat.getString(1));
				jo.put("date", resultat.getString(2));// 3
				events.put(jo);
			}
			for (int i = 0 ; i < events.length(); i++) {
				JSONObject obj = events.getJSONObject(i);
				String event_id = obj.getString("event_id");
				String dt2 = obj.getString("date");
			    date1=new SimpleDateFormat("yyyy-MM-dd").parse(dt2); 
				
			    if(dt1.after(date1)) {
					//it's expired
					boolean f=SetStatus(event_id);
					System.out.println(f);
				}
			 }
			ret=Tools.ErrorJson.serviceAccepted();
			resultat.close();
			statement.close();
			connexion.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} catch (ParseException p) {
			p.printStackTrace();
		}

		return ret;
	}
	
	/**
	 * SetStatus modifie ses status pour ne soit pas active
	 * @param idEvent
	 * @return JSONObject code
	 */
	public static Boolean SetStatus(String idEvent){
		Connection connexion;
		boolean resultat=false;
		try {
		connexion = Database.getMySQLConnection();
		Statement statement = connexion.createStatement();
		String query ="UPDATE events SET status = 0 WHERE event_id="+idEvent;
		resultat = statement.execute(query);
		statement.close();
		connexion.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
		return resultat;
		
	}

	
	
}
