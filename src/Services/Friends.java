package Services;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import BaseDD.Database;
import Tools.ConnexionTools;
import Tools.ErrorJson;
import Tools.FriendTools;
import Tools.UserTools;
/**
 * services relié à la gestion de Friends
 * 
 * @author Nassima GHOUT et Shokoufeh AHMADI SIMAB
 *
 */
public class Friends {
	/**
	 * ********ADD_FRIEND ajouter contrainte d ene pas s'ajouter soi meme verifer
	 * idfriend!=mon id
	 * 
	 * @param key
	 * @param id_friend
	 * @return
	 * 
	 *         Entree : key + id_friend Sortie : {}
	 */
	public static JSONObject AddFriend(String key, String id_fr) {
		JSONObject retour = new JSONObject();
		String login = UserTools.getLogSession(key);
		int my_id = ConnexionTools.getId(login);

		try {
			if (key == null || id_fr == null)
				return ErrorJson.serviceRefused("pas d'argument", -1);
			if (!ConnexionTools.checkSession(key))
				return ErrorJson.serviceRefused("Connexion non existante", 100);
			int id_friend = Integer.parseInt(id_fr);
			if (my_id == id_friend)
				return ErrorJson.serviceRefused("id incorrect", 100);
			if (!ConnexionTools.checkId(id_friend))
				return ErrorJson.serviceRefused("id friend not exists", 1000);
			if (FriendTools.checkFriend1(my_id, id_friend))
				return ErrorJson.serviceRefused("Operation impossible, vous etes deja amis ", 2);
			if (FriendTools.checkFriend1(id_friend, my_id))
				return ErrorJson.serviceRefused("Operation impossible, vous etes deja amis ", 2);
			FriendTools.InsertFriend(key, id_friend);
			retour = Tools.ErrorJson.serviceAccepted();
		} catch (JSONException j) {
			j.printStackTrace();
		}
		return retour;
	}

	/**
	 * ******REMOVE_FRIEND Entree : key + id_friend Sortie : {}
	 *
	 * @param key
	 * @param id_friend
	 * @return
	 */
	public static JSONObject RemoveFriend(String key, String id_fr) {
		JSONObject retour = new JSONObject();

		try {
			if ((key == null) || (id_fr == null))
				return Tools.ErrorJson.serviceRefused("missing arguments", -1);
			if ((key == null) || (!ConnexionTools.checkSession(key)))
				return Tools.ErrorJson.serviceRefused("Connexion non existante", -1);
			int id_friend = Integer.parseInt(id_fr);
			if (!FriendTools.checkFriend(id_friend))
				return Tools.ErrorJson.serviceRefused("id friend non existant", -1);

			Connection connexion = (Connection) Database.getMySQLConnection();
			java.sql.Statement statement = connexion.createStatement();
			String query = "DELETE FROM friends WHERE id_user1='" + FriendTools.getId(key) + "'and id_user2='"
					+ id_friend + "';";

			statement.executeUpdate(query);
			statement.close();
			connexion.close();

			retour = Tools.ErrorJson.serviceAccepted();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (SQLException s) {
			s.printStackTrace();
		}
		return retour;
	}

	/**
	 * lister les amis d'un utilisateur
	 * 
	 * @param login
	 * @return
	 */
	public static JSONArray listFriend(String key) {
		JSONArray friends = new JSONArray();
		
		//int id_user1 = ConnexionTools.getId(login);
		try {
			if (key == null) 
				return friends.put(Tools.ErrorJson.serviceRefused("Pas d'argument", -1));
			if (!ConnexionTools.checkSession(key))
				return friends.put(Tools.ErrorJson.serviceRefused("connexion not existante", 1));
			
			String login = UserTools.getLogSession(key);
			Connection connexion;

			connexion = (Connection) Database.getMySQLConnection();
			Statement statement = (Statement) connexion.createStatement();
			int id_user1 = ConnexionTools.getId(login);
			String query = "SELECT id_user2, follow_date FROM friends WHERE id_user1 = '" + id_user1 + "' ";
			
			ResultSet resultat = statement.executeQuery(query);
			while (resultat.next()) {
				// System.out.println("nom::"+resultat.getString(3));
				JSONObject jo = new JSONObject();
				jo = Tools.ErrorJson.serviceAccepted();
				jo.put("Statue", "0K");
				jo.put("code", 200);
				jo.put("id_friend", resultat.getString(1));// 
				System.out.println("string 1  "+resultat.getString(1));
				System.out.println("string 2  "+resultat.getString(2));

				jo.put("date",resultat.getString(2));
				jo.put("login", UserTools.getLogin(Integer.parseInt(resultat.getString(1))));// ok
			
				//jo.put("follow_date", resultat.getString(4));// 
				friends.put(jo);
				
			}
			resultat.close();
			statement.close();
			connexion.close();

		} catch (JSONException j) {
			j.printStackTrace();
		} catch (SQLException s) {
			s.printStackTrace();
		}
		System.out.println(friends);
		return friends;
	}

	/**
	 * récupérer tout les utilisateur inscrit à l'appli
	 * 
	 */
	public static JSONObject getUsers(String login) {
		int id_user1 = ConnexionTools.getId(login);
		JSONObject jo = new JSONObject();
		Connection connexion;

		try {
			if (!ConnexionTools.checkId(id_user1))
				return Tools.ErrorJson.serviceRefused("Utilisateur not exists", -1);

			connexion = (Connection) Database.getMySQLConnection();
			Statement statement = (Statement) connexion.createStatement();
			String query = "SELECT * FROM user ";
			ResultSet resultat = statement.executeQuery(query);
			jo.append("Statue", "OK");
			jo.append("code", 200);
			while (resultat.next()) {
				jo.append("login", UserTools.getLogin(Integer.parseInt(resultat.getString(1))));
				jo.append("user_id", resultat.getString(1));

			}
			resultat.close();
			statement.close();
			connexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		}
		return jo;

	}

	/**
	 * compter les nombres des amies
	 * 
	 */
	public static JSONObject friendsCount(String key) {
		// TODO Auto-generated method stub
		JSONObject jo = new JSONObject();
		try {
			if (key == null) 
				return Tools.ErrorJson.serviceRefused("Pas d'argument", -1);
			if (!ConnexionTools.checkSession(key))
				return Tools.ErrorJson.serviceRefused("connexion not existante", 1);
			
			String login = UserTools.getLogSession(key);
			int idUser= UserTools.getUserId(login);
			Connection connexion;
			connexion = (Connection) Database.getMySQLConnection();
			Statement statement = (Statement) connexion.createStatement();
			String query = "SELECT COUNT(*) FROM friends WHERE id_user1="+idUser+"";
			ResultSet resultat = statement.executeQuery(query);
			System.out.println(">>>"+resultat);
			jo.append("Statue", "OK");
			jo.append("code", 200);
			while (resultat.next()) {
				jo.append("FriendNum",resultat.getString(1));

			}
			resultat.close();
			statement.close();
			connexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		}
		return jo;
	}

}
