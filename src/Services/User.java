package Services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import Tools.ConnexionTools;
import Tools.FriendTools;
import Tools.UserTools;
import BaseDD.Database;
	/**
	 * Services reli� � la gestion d'un utilisateur
	 * 
	 * @author Shokoufeh AHMADI SIMAB et Nassima GHOUT
	 *
	 */

public class User {

	/**
	 * CREATE_USER Creation et insertion d'un utilisateur dans la BD Verification
	 * que les arguments sont pas null et que login n'existe pas d�j�
	 * 
	 * @param login
	 * @param password
	 * @param nom
	 * @param prenom
	 * @param sexe
	 * @param agei
	 * @param mail
	 * @param phone
	 * @param poidsi
	 * @param taillei
	 * @param adress
	 * @param zipcode
	 * @return
	 * @throws ParseException 
	 */

	public static JSONObject createUser(String login, String password, String pass2,String nom, String prenom, String sexe,
			String agei, String mail, String phone, String poidsi, String taillei, String adress, String zipcode ,String country) {
		JSONObject retour = new JSONObject();

		try {
			/**
			 * verification des arguments 
			 */
			if ((login == null) || (mail == null) || (password == null) || (pass2 == null) ||  (prenom == null) || (nom == null)
					|| (sexe == null) || (agei == null) || (taillei == null) || (mail == null) || (phone == null)
					|| (poidsi == null || (adress == null))) 
				return Tools.ErrorJson.serviceRefused("missing  arguments ", -1);
			if(!password.equals(pass2))
				return Tools.ErrorJson.serviceRefused("two pass are not same", -11);
			/**
			 * v�rifier que le login n'existe pas d�j�
			 */
			
			if (Tools.ConnexionTools.checkUser(login))
				return Tools.ErrorJson.serviceRefused("Login already exists", 1);
			if(Tools.ConnexionTools.checkEmail(mail))
				return Tools.ErrorJson.serviceRefused("this Email has an account", 12);
			int poids, taille;
			double bmi;
			try {
				poids = Integer.parseInt(poidsi);
				System.out.println(poids);
				taille = Integer.parseInt(taillei);
				bmi = calculatebmi(poids, taille);
			} catch (NumberFormatException e) {
				e.printStackTrace();
				return Tools.ErrorJson.serviceRefused("number format exception ", -2);
			}
			//appel methode d'insertion dans la base
			
			System.out.println("date de naissance"+agei);
			try {
				Tools.UserTools.InsertUser(login, password, nom, prenom, sexe, agei, mail, phone, poids, taille, bmi, adress,
						zipcode,country);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			retour = Tools.ErrorJson.serviceAccepted();
		} catch (SQLException s) {
			s.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return retour;
	}

	/**
	 * Calculer BMI d'un utilisateur den prennat la taille et poids
	 * 
	 * @param poids
	 * @param taille
	 * @return Bmi
	 */
	private static double calculatebmi(int poids, int taille) {
		double newtaille = (float) taille / 100;
		double bmi = poids / (newtaille * newtaille);
		return bmi;
	}

	/**
	 * Connexion, verification des arguments et appel methode d'insertion de la
	 * connexion dans la table "session" de la base de donn�e
	 * 
	 * @param login
	 * @param password
	 * @return {msg,code,login,key} dnas le cas o� connexion r�ussi sinon {msg,code}
	 *         //service refus�
	 */
	public static JSONObject connexion(String login, String password) {
		JSONObject retour = new JSONObject();
		try {
			if ((login == null) || (password == null))
				return Tools.ErrorJson.serviceRefused("missing arguments", -1);
			/**
			 * on vrifie que le login existe dans la base (utilisateur inscrit)
			 */
			if (!ConnexionTools.checkUser(login))
				return Tools.ErrorJson.serviceRefused("User not exists", 1);
			/**
			 * on vrifie qu'il a rentr le bon mot de passe
			 */
			if (!ConnexionTools.checkLogPwd(login, password))
				return Tools.ErrorJson.serviceRefused("Wrong password", 2);
			/**
			 * vrifier utilisateur n'est pas dj connect
			 */
		/*	if (ConnexionTools.UserConnected(login)) {
				System.out.println(">>>>>>>>");
				return Tools.ErrorJson.serviceAccepted();
			}	//return Tools.ErrorJson.serviceRefused("User already connected", 200);
			/**
			 * insrtion  la base et rcupration la cl de connexion
			 */
			if(ConnexionTools.UserConnected(login)) {
				Connection connexion = Database.getMySQLConnection();
				Statement statement = connexion.createStatement();

				String query ="DELETE FROM session WHERE user_login= '" + login + "';";
				statement.executeUpdate(query);
				statement.close();
				connexion.close();
			}
			String key = UserTools.InsertConnexion(login);
			// ajout la cle de connexion et le login dans le retour
			retour = Tools.ErrorJson.serviceAccepted();
			retour.put("login", login);
			retour.put("key", key);

		} catch (JSONException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retour;
	}

	/**
	 * DECONNEXION v�rification des arguments: 1- non null 2- la cl� existe dans la
	 * base (utilisateur connect�) 3- suppression de la ligne dan sla base de donn�e
	 * 
	 * @return {msg,code}
	 *
	 */
	public static JSONObject logout(String key) {
		JSONObject retour = new JSONObject();

		try {
			if (key == null)
				return Tools.ErrorJson.serviceRefused("missing arguments", 10);
			if (!ConnexionTools.checkSession(key))
				return Tools.ErrorJson.serviceRefused("invalid key", 100);

			Connection connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String query = "DELETE FROM session WHERE session_key='" + key + "';";
			statement.executeUpdate(query);
			statement.close();
			connexion.close();
			retour = Tools.ErrorJson.serviceAccepted();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (SQLException s) {
			s.printStackTrace();
		}
		return retour;
	}
	/**
	 * listUser retourner list des users
	 * @param key
	 * 
	 * @return JSONArray
	 *
	 */
	public static JSONArray listUsers(String key) {
		JSONArray users = new JSONArray();
	//	JSONObject retour = new JSONObject();
		try {
			if (key == null) 
				return users.put(Tools.ErrorJson.serviceRefused("Pas d'argument", -1));
			if (!ConnexionTools.checkSession(key))
				return users.put(Tools.ErrorJson.serviceRefused("connexion not existante", 1));
			String login = UserTools.getLogSession(key);
			int idUser=UserTools.getUserId(login);
			Connection connexion;
			connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String query = "SELECT * FROM user where user_id !="+idUser; //all users
			ResultSet resultat = statement.executeQuery(query);			
			while (resultat.next()) {
				JSONObject jo = new JSONObject();
				jo = Tools.ErrorJson.serviceAccepted();
				int ff= Integer.parseInt(resultat.getString(1));
				System.out.println("idUser2"+ff+"--"+FriendTools.checkFriend1(idUser,ff));
				//check if user is not in her/his friends list 
			
				if(FriendTools.checkFriend1(idUser,ff)==false && ff!=idUser ) {
					jo.put("user_id", resultat.getString(1));
					jo.put("user_login", resultat.getString(2));
					users.put(jo);
				}
			}
			resultat.close();
			statement.close();
			connexion.close();
		} catch (JSONException j) {
			j.printStackTrace();
		} catch (SQLException s) {
			s.printStackTrace();
		}
		return users;
	}

	public static JSONObject UserInfo(String userId, String key) {
		// TODO Auto-generated method stub
		JSONObject usersInfo = new JSONObject();
		try {
			if (key == null) 
				return Tools.ErrorJson.serviceRefused("Pas d'argument", -1);
			if (!ConnexionTools.checkSession(key))
				return Tools.ErrorJson.serviceRefused("connexion not existante", 1);
			String loginViewer = UserTools.getLogSession(key);
			int idUserViewer =UserTools.getUserId(loginViewer);
			//int idUserMain=UserTools.getUserId(log);
			System.out.println(userId);
			int user_id=Integer.parseInt(userId);
			Connection connexion;
			JSONObject jo = new JSONObject();
			connexion = Database.getMySQLConnection();
			
			Statement statement = connexion.createStatement();
			String query = "SELECT * FROM user WHERE user_id="+user_id+""; //all users
			ResultSet resultat = statement.executeQuery(query);			
			while (resultat.next()) {
				if(FriendTools.checkFriend1(idUserViewer, user_id)) {
						jo.put("state", "OK");
						jo.put("code", 200);
						jo.put("user_id", resultat.getString(3));
						jo.put("nom", resultat.getString(5));// 3
						jo.put("prenom", resultat.getString(4));// 4
						jo.put("sexe", resultat.getString(6));// 4
						jo.put("age", resultat.getString(7));// 4
						jo.put("mail", resultat.getString(8));// 5
						jo.put("phoneNumber", resultat.getString(9));// 6
						//jo.put("poids", resultat.getString(10));// 6
						//jo.put("taille", resultat.getString(11));// 6
						//jo.put("bmi", resultat.getString(12));// 6
						//jo.put("adress", resultat.getString(13));// 6
						//jo.put("ZipCode", resultat.getString(14));// 6
						jo.put("Country", resultat.getString(15));
						jo.put("Friendship", "yes");
				}
				else {
					jo.put("state", "OK");
					jo.put("code", 200);
					jo.put("nom", resultat.getString(5));// 3
					jo.put("prenom", resultat.getString(4));// 4
					jo.put("Country", resultat.getString(15));
					jo.put("Friendship", "no");
				}

			}
			resultat.close();
			statement.close();
			connexion.close();
			usersInfo = jo;
			return jo;
		} catch (JSONException j) {
			j.printStackTrace();
		} catch (SQLException s) {
			s.printStackTrace();
		}
		
		return usersInfo;
	}

	public static JSONArray getNotifications(String key) throws SQLException {
		JSONArray notifs2 = new JSONArray();
	
		try {
			if (key == null) 
				return notifs2.put(Tools.ErrorJson.serviceRefused("Pas d'argument", -1));
			if (!ConnexionTools.checkSession(key))
				return notifs2.put(Tools.ErrorJson.serviceRefused("connexion not existante", 1));
			String owner = UserTools.getLogSession(key);
			int idUser=UserTools.getUserId(owner);
			int cmp=0;
			Connection connexion;
			connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String query = "SELECT * FROM events where owner ='"+owner+"' OR user_id="+idUser; //all users
			//System.out.println(query);
			ResultSet resultat = statement.executeQuery(query);			
			while (resultat.next()) {
				JSONObject pf = new JSONObject();
				//controle pour savoir si la date est proche
				if(Events.NearDate(resultat.getString(6))) {
					pf = Tools.ErrorJson.serviceAccepted();
					pf.put("event_name", resultat.getString(4));
					pf.put("date_rest", resultat.getString(6));
					
					pf.put("notifs_compt", cmp);
					System.out.println("pf"+pf);
				//	jo.put("owner", resultat.getString(3));
					notifs2.put(pf);
				}else {
					System.out.println("there isn't any notif ");
				}
				
				}
		
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		System.out.println("notifs="+notifs2.length());
		return notifs2;
	}
}