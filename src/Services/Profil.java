package Services;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Redirect;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Duration;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import BaseDD.Database;
import Tools.ConnexionTools;
import Tools.UserTools;

/**
 * Services reli� � la gestion de profil
 * 
 * @author Nassima GHOUT et Shokoufeh AHMADI SIMAB
 *
 */

public class Profil {

	/**
	 * recuperer les infos personnelles d'un utilisateur pour les afficher dans sa
	 * page de profil
	 * 
	 * @param key
	 * @return
	 */
	public static JSONObject getProfil(String key) {
		JSONObject retour = new JSONObject();
		try {
			if (key == null)
				return Tools.ErrorJson.serviceRefused("Pas d'argument", -1);
			if (!ConnexionTools.checkSession(key))
				return Tools.ErrorJson.serviceRefused("connexion not existante", 1);
			String login = UserTools.getLogSession(key);
			int userId=UserTools.getUserId(login);
			System.out.println(userId);
			Connection connexion;
			JSONObject jo = new JSONObject();
			connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String query = "SELECT * FROM user WHERE user_id= " + userId + " ";
			
			ResultSet resultat = statement.executeQuery(query);
			while (resultat.next()) {
				System.out.println("testtt");
				jo.put("state", "OK");
				jo.put("code", 200);
				jo.put("user_id", resultat.getString(3));
				jo.put("nom", resultat.getString(5));// 3
				jo.put("prenom", resultat.getString(4));// 4
				jo.put("sexe", resultat.getString(6));// 4
				jo.put("age", resultat.getString(7));// 4
				jo.put("mail", resultat.getString(8));// 5
				jo.put("phoneNumber", resultat.getString(9));// 6
				jo.put("poids", resultat.getString(10));// 6
				jo.put("taille", resultat.getString(11));// 6
				jo.put("bmi", resultat.getString(12));// 6
				jo.put("adress", resultat.getString(13));// 6
				jo.put("ZipCode", resultat.getString(14));// 6
				jo.put("Country", resultat.getString(15));

			}
			resultat.close();
			statement.close();
			connexion.close();
			retour = jo;
			return jo;

		} catch (JSONException j) {
			j.printStackTrace();
		} catch (SQLException s) {
			s.printStackTrace();
		}
		return retour;

	}	
	/**
	 * calculer l'indice de bmi d'utilsateur
	 * @param poids,taille
	 * @return bmi
	 * @throws JSONException
	 */
	private static double calculatebmi(int poids, int taille) {
		double newtaille = (float) taille / 100;
		double bmi = poids / (newtaille * newtaille);
		return bmi;
	}
	/**
	 * Mise a jour les info personnel
	 * @param 
	 * @return code 
	 * @throws JSONException
	 */
	public static JSONObject updateInfo(String key ,String nomNew, String prenomNew, String sexeNew,
			String ageNew, String mailNew, String phoneNew, String poidsNew, String tailleNew, String adressNew, String zipcodeNew ,String countryNew) {
		
		JSONObject retour = new JSONObject();
		if (key == null)
			try {
				return Tools.ErrorJson.serviceRefused("Pas d'argument", -1);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		if (!ConnexionTools.checkSession(key))
			try {
				return Tools.ErrorJson.serviceRefused("connexion not existante", 1);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		String LogIN = UserTools.getLogSession(key);
		Connection connexion;
		JSONObject jo = new JSONObject();
		try {
			connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			int age = Integer.parseInt(ageNew);
			int poids = Integer.parseInt(poidsNew);
			int taille = Integer.parseInt(tailleNew);
			double bmiNew = calculatebmi(poids, taille);
			String query = "UPDATE user SET nom='"+nomNew+"', prenom='"+prenomNew+"',sexe='"+sexeNew+"',age="+age+",mail='"+mailNew+"',phoneNumber='"+phoneNew+"',poids="+
			poids+",taille="+taille+",bmi="+(int)bmiNew+",adresse='"+adressNew+"',ZipCode='"+zipcodeNew+"',Country='"+countryNew+"' WHERE user_login= '" + LogIN + "' ";
			System.out.println(">>>"+query);
			boolean resultat = statement.execute(query);
			System.out.println(">>>result ::"+resultat);
			statement.close();
			connexion.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		retour = jo;
		return jo;
		

	}

	/**
	 * 
	 * 
	 * @param get zipcode
	 * @param key
	 * @param appid
	 * @return
	 */
	public static String findZip(String key) {
		String zip = "";

		try {
			String login = UserTools.getLogSession(key);
			Connection connexion;

			connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String query = "SELECT ZipCode FROM user WHERE user_login= '" + login + "' ";
			ResultSet resultat = statement.executeQuery(query);
			while (resultat.next()) {
				zip = resultat.getString(1);
			}

			statement.close();
			connexion.close();
			return zip;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return zip;
	}
	/**
	 *trouver le payee de user
	 * @param key
	 * @return country 
	 * @throws JSONException
	 */
	public static String findCountry(String key) {
		String country = "";
		String login = UserTools.getLogSession(key);
		Connection connexion;
		try {
			connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String query = "SELECT country FROM user WHERE user_login= '" + login + "' ";
			ResultSet resultat = statement.executeQuery(query);
			System.out.println(resultat);
			while (resultat.next()) {
				country = resultat.getString(1);
			}

			statement.close();
			connexion.close();
			return country;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return country;
	}

	/**
	 * Api de Meteo qui recupere la meteo
	 * @param key,appid
	 * @param 
	 * @return meteo
	 */
	public static JSONObject ShowWeather(String key, String appid) {

		JSONObject res = new JSONObject();
		// String country = "fr";
		String zip = findZip(key);
		System.out.println(key);
		try {
			if (! ConnexionTools.checkSession(key))
				return Tools.ErrorJson.serviceRefused("invalid session ", -10);
			
			String country = findCountry(key);
			System.out.println(country);
				HttpClient client = HttpClient.newBuilder().version(Version.HTTP_1_1).followRedirects(Redirect.NORMAL)
					.connectTimeout(Duration.ofSeconds(20)).build();
			HttpRequest request = HttpRequest.newBuilder().uri(URI.create(
					"https://api.openweathermap.org/data/2.5/weather?zip=" + zip + "," + country + "&appid=" + appid))
					.build();
				
			HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
			JSONObject jo = new JSONObject(response.body());
			String temptMin = jo.getJSONObject("main").getString("temp_min");
			String temptMax = jo.getJSONObject("main").getString("temp_max");
			String locationLong = jo.getJSONObject("coord").getString("lon");
			String locationLat = jo.getJSONObject("coord").getString("lat");

			// take weather
			JSONArray arr = jo.getJSONArray("weather");

			String desc = "";
			String main;
			for (int i = 0; i < arr.length(); i++) {
				desc = arr.getJSONObject(i).getString("description");
				main = arr.getJSONObject(i).getString("main");
			}
			res.put("code", 200);
			res.put("description", desc);
			res.put("tempMax", temptMax);
			res.put("tempMin", temptMin);
			res.put("lat", locationLat);
			res.put("lng", locationLong);
			return res;

		} catch (JSONException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	//	return res;
				return null;
	}

	/**
	 * method for taking the user place
	 * 
	 * @param lat
	 * @param lng
	 * @param loc
	 * @return list des location
	 */
	public static JSONArray getLocation(String lat, String lng, String loc) {
		JSONObject locs = new JSONObject();
		
		JSONArray ret = new JSONArray();

		try {
			if ((lat == null) || (lng == null) || (loc == null))
				return ret.put(Tools.ErrorJson.serviceRefused("Pas d'argument", -1));

			HttpClient client = HttpClient.newBuilder().version(Version.HTTP_1_1).followRedirects(Redirect.NORMAL)
					.connectTimeout(Duration.ofSeconds(20)).build();
			System.out.println("before request");
			HttpRequest request = HttpRequest.newBuilder()
					.uri(URI.create("https://places.ls.hereapi.com/places/v1/autosuggest?at=" + lat + "," + lng + "&q="
							+ loc + "&apikey=Qqj8aLe3qA02u0-qmAxd3iaKJFZnaggZ7ifZFEuTQBw"))
					.build();
	
			HttpResponse<String> response;
			
			response = client.send(request, BodyHandlers.ofString());
			JSONObject myObject = new JSONObject(response.body());
			// String str4 = (String) myObject.remove("{\"result\":");
			locs =myObject;
			Object obj = locs.get("results");
			if(obj instanceof JSONArray) {
				ret = (JSONArray) obj;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	
		return ret;

	}

	/**
	 * Api qui retourne list des exercise
	 * @param pageNum
	 * @return
	 * @throws JSONException
	 */


	public static JSONArray exerciseInfo(String pageNum) throws JSONException {
		JSONObject res = new JSONObject();
		JSONArray ret = new JSONArray();

		HttpClient client = HttpClient.newBuilder().version(Version.HTTP_1_1).followRedirects(Redirect.NORMAL)
				.connectTimeout(Duration.ofSeconds(20)).build();
		HttpRequest request = HttpRequest.newBuilder().uri(URI.create(
				"http://wger.de/api/v2/exercise/?language=2&page=" + pageNum + "&apikey=834afdff275b267b3b652905a7e1089846cbd8b7"))
				.build();
		HttpResponse<String> response;
		try {
			response = client.send(request, BodyHandlers.ofString());
			JSONObject myObject = new JSONObject(response.body());
			res =myObject;
			Object obj = res.get("results");
			if(obj instanceof JSONArray) {
				ret = (JSONArray) obj;
				
				System.out.println("ret :: " +ret.toString());
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return ret;

	}
	/**
	 * Analysis de sante
	 * @param key
	 * @return JSONObject
	 * @throws JSONException
	 */

	public static JSONObject WeightAnalysis(String key) throws JSONException {
		JSONObject res = new JSONObject();
		int bmi=0;
		String login = UserTools.getLogSession(key);
		Connection connexion;
		if(key==null) {
			return Tools.ErrorJson.serviceRefused("Pas d'argument", -1);
		}
		try {
			connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String query = "SELECT bmi FROM user WHERE user_login= '" + login + "' ";
			ResultSet resultat = statement.executeQuery(query);
			System.out.println(resultat);
			while (resultat.next()) {
				String sbmi= resultat.getString(1);
				bmi=Integer.parseInt(sbmi);
			}
			
			statement.close();
			connexion.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(bmi<18.5) {
			res.put("Status", "underWeight");
			res.put("description"," you have a low amount of body fat. If you are an athlete, this can be desirable. If you are not an athlete, a lean BMI can indicate that your weight may be too low, which may lower your immunity. If your BMI and body weight are low, you should consider gaining weight through good diet and exercise habits, to increase your muscle mass.");
		}
		if(bmi>18.5 && bmi<24.9) {
			res.put("Status", "NormalWeight");
			res.put("description", "This is still considered an acceptable range, and is associated with good health.");
		}
		if(bmi>25 && bmi<29.9) {
			res.put("Status", "OverWeight");
			res.put("description", "you should finds ways to lower your weight, through diet and exercise. You are at increased risk for a variety of illnesses at your present weight. You should lose weight by changing your diet and exercising more.");
		}
		if(bmi>30)
		{	
			res.put("Status", "obesity");
			res.put("description", "This indicates an unhealthy condition, your excess mass is putting you at risk for heart disease, diabetes, high blood pressure, gall bladder disease, circulation problems, and some cancers. You should lose weight by changing your diet and exercising more.");
		}
		return res;
	}
	/**
	 * Sauvgarder Avatar de user
	 * @param key 
	 * @param path
	 * @return JSONObject
	 * @throws JSONException
	 */
	public static JSONObject SaveAvatar(String key, String path) throws JSONException {
		JSONObject retour = new JSONObject();
		if (key == null)
			try {
				return Tools.ErrorJson.serviceRefused("missing arguments", -1);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		if (!ConnexionTools.checkSession(key))
			return Tools.ErrorJson.serviceRefused("Connexion non existante", -10);
		Connection connexion;
		try {
			connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String login = UserTools.getLogSession(key);
			int user_id=UserTools.getUserId(login);
			String query = "INSERT INTO avatar(user_id,path) VALUES(" + user_id+ ",'" + path+"');";
			statement.executeUpdate(query);
			statement.close();
			connexion.close();

			retour = Tools.ErrorJson.serviceAccepted();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return retour;
	}
	
}
